/* Activity:
1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
- If the total of the two numbers is less than 10, add the numbers
- If the total of the two numbers is 10 - 20, subtract the numbers
- If the total of the two numbers is 21 - 29 multiply the numbers
- If the total of the two numbers is greater than or equal to 30, divide the numbers
4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
5. Prompt the user for their name and age and print out different alert messages based on the user input:
-  If the name OR age is blank/null, print the message are you a time traveler?
-  If the name AND age is not blank, print the message with the user’s name and age.
6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
- 18 or greater, print an alert message saying You are of legal age.
- 17 or less, print an alert message saying You are not allowed here.
8. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
- 18 - print the message You are now allowed to party.
- 21 - print the message You are now part of the adult society.
- 65 - print the message We thank you for your contribution to society.
- Any other value - print the message Are you sure you're not an alien?
8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.
9. Create a git repository named S15.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle. */

/* console.log("Hello World")

let x = parseInt(prompt("Provide a number"));
let y = parseInt(prompt("Provide another number"));

if (x + y <10){
  console.warn(x+y);
}
else if (x + y >= 10 && x + y <=20){
  alert(x-y);
}
else if (x + y >20 && x + y <30){
  alert (x*y);
}
else if (x+ y >= 30){
  alert(x/y);
}; */

let yourName = prompt("What is your name?");
let yourAge = prompt("What is your age")

console.log(yourName);
console.log(yourAge);
let nameOrAge = yourName && yourAge;
let nameAndAge = yourName && yourAge;


if (nameOrAge == "" | null) {
  alert("Are you a time traveller?")
}
else if (nameAndAge != "" | null) {
  alert(`Hello ${yourName}. Your age is ${yourAge}`)
}

function isLegalAge(){
  if(yourAge>=18){
    alert("You are of legal age.")
  }
  else{
    alert("You are not allowed here")
  }
}
isLegalAge(yourAge)

switch (true) {
  case yourAge>=18 && yourAge<21:
    alert("You are now allowed to party")
    break;

  case yourAge>18 && yourAge>=21 && yourAge<65:
    alert("You are now part of the adult society.")
    break;

  case yourAge>=65:
    alert("We thank you for your contribution to society.")
    break;

  default:
    alert("Are you sure you are not an alien?")
    break;
}

function legalAgeCheck(yourAge){
  try{
    alert(isLegalAg(yourAge));
  }
  catch(error){
    console.log(typeof error)
    console.warn("isLegalAg is not defined");
  }
  finally{
    alert("There is an error")
  }
}
legalAgeCheck(yourAge)