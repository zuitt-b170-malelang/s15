// Mathematical Operators
/* 
  + Sum
  - Difference
  * Product
  / Quotient

  Used when the value of the first number is stored in a variable:
  performs the operation as well as storing the result as the new value for the variable
  += Addition
  -= Subtraction
  *= Multiplication
  /= Division
  % or %= modulo (get the remainder)

*/
function mod() {
  // return 9+2
  // return 9-2
  // return 9*2
  // return 9/2
  let x = 9
  return x %= 2
};

console.log(mod());

// Assignment Operator (=) used for assignening values to variables
let x = 1;
let sum = 1;
sum += x
console.log(sum);

// Increment v& Decrement

// pre-increment: add 1 before assigning the value to the variable

let z = 1;
let increment = ++z;
console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);

// Post increment - adding 1 AFTER assigning the value of the variable
increment = z++;
console.log(`Result of post-increment: ${increment}`);
console.log(`Result of post-increment: ${z}`);

// Decrement

// Predecrement - -1 before the assigning of value
let decrement = --z;
console.log(`Result of post-increment: ${decrement}`);
console.log(`Result of post-increment: ${z}`);

// post-decrement
decrement = z--;
console.log(`Result of postdecrement: ${decrement}`);
console.log(`Result of postdecrement: ${z}`);


// Comparison Operators
// Equalitty Operator (==) - compares if the two values are equal
let juan = "juan";
console.log(1 == 1);
console.log(0 == false);
console.log(juan == "juan");

// Strict Equality (===)
// the javascript is not anymore concerned with other relative functions of the data, it only compares the two values as they are.
console.log(1 === 1);

// Inequality (!=)
console.log(1 != 1);
console.log(juan != "Juan")

// Strict inequality (!==)
console.log(0 !== false)

// Other Comparison Operators
/* 
  > Greater than
  < Less than
  >= Greater than or equal
  <= Less than or equal


*/

// LOGICAL OPERATORS
/* 
  AND Operator (&&) - returns true if all operands are true

  OR Operator (||)
*/



// And (&&) Operator
let isLegalAge = true;
let isRegistered = false;

let allRequirementMet = isLegalAge && isRegistered
console.log(`Result of logical AND operator: ${allRequirementMet}`);

// OR Operator
allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementMet}`)

// Selection Control Structures

/* 
  IF Statements - execute a command if a specified condition is true
  SYNTAX:
   if(condition){
     <statements/command>
   }
*/

let num = -1;

if (num < 0) {
  console.log("Hello")
};

let value = 30;

if (value > 10) {
  console.log(`Welcome to Zuitt`)
};

/* 
  if-else Statement - executes a command if the first condition returns false
    SYNTAX
    if (condition){
      <command/statement>
    }
    else{
      <statement/command if false>
    };
*/

num = 5;
if (num > 10) {
  console.log(`Number is greater than 10`)
}
else {
  console.log(`Number is less than 10`)
};

// prompt - dialog box that has input fields
// alert - dialog box that has no input field; used for warnings, announcements, etc.
// parseInt - converts a number in string data into a numerical data type. It only recognizes numbers and ignores alphabets (it returns NaN - Not a Number)
num = parseInt(prompt("Please input a number."));

if (num > 59) {
  alert("Senior Age")
  console.log(num)
}
else {
  alert("Invalid Age")
  console.log(num)
};

// if-elseif-else Statement

/* 
  1. Quezon
  2. Valenzuela
  3. Pasig
  4. Taguig
*/

let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
  alert("Welcome to Quezon City")
}
else if (city === 2) {
  alert("Welcome to Valenzuela City")
}
else if (city === 3) {
  alert("Welcome to Pasig City")
}
else if (city === 4) {
  alert("Welcome to Taguig City")
}
else {
  alert("Invalid Number")
};

let message = "";

function determineTyphoonIntensity(windspeed) {
  if (windspeed < 30) {
    return `Not a typhoon yet.`
  }
  else if (windspeed <= 61) {
    return `Tropical Depression detected.`
  }
  else if (windspeed >= 62 && windspeed <= 88) {
    return `Tropical Storm detected.`
  }
  else if (windspeed >= 89 && windspeed <= 117) {
    return `Severe Tropical Storm detected`
  }
  else {
    return `Typhoon detected`
  }
};
message = determineTyphoonIntensity(120);
console.log(message);

/* 
  Ternary Operator
  SYNTAX:
  (Conditoion)?ifTrue:ifFalse
*/


let ternaryResult= (1<18)?true:false;
console.log(`Result of ternary operator: ${ternaryResult}`)

let name;
function isOfLegalAge(){
	name = "John";
	return "You are of the legal age Limit"
};
// .............
function isUnderAge(){
	name = "Jane";
	return "You are under the legal Age"
};
let age = parseInt(prompt("What is Your Age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();

alert( `Result of Ternary Operator in Function: ${LegalAge} ${name}` );

// Switch Statement - shorthand for if-elsif-else should there be a lot of conditions

// break - signals the broweser when the case value has been met, no need to go further down the command
/* 
SYNTAX:
switch (expression/parameter){
    case value1:
    statement/s;
    break;
    case value2:
    statement/s;
    break;
    case value3:
    statement/s;
    break;
    case value4:
    statement/s;
    break;
    case value5:
    statement/s;
    break;
    case value6:
    statement/s;
    break;
    .
    .
    .
    .
    default:
      statement/s;
}
*/
/* let day = prompt("What is it today?").toLowerCase();
switch (day){
  case "sunday":
    alert("The color of the day is red");
    break;
  case "monday":
    alert("The color of the day is orange");
    break;
  case "tuesday":
    alert("The color of the day is yellow");
    break;
  case "wednesday":
    alert("The color of the day is green");
    break;
  case "thursday":
    alert("The color of the day is blue");
    break;
  case "friday":
    alert("The color of the day is violet");
    break;
  case "saturday":
    alert("The color of the day is indigo");
    break;
  default:
    alert("Please input a valid day");
}
 */

// Try-Catch-Finally Statement - commonly used for error handling

function showIntensityAlert(windspeed){
  try{
    alert(determineTyphoonIntensity(windspeed));
  }
  catch(error){
    console.log(typeof error)
    console.warn(error.message);
  }
  finally{
    alert("intensity updates will show new alert")
  }
}
showIntensityAlert(56)